import turtle
import random

class Room:
    def __init__(self,gridsize,k):
        self.grid = gridsize
        self.k = k
        
    def drawRoom(self):
        self.window = turtle.Screen()
        m = self.grid
        self.window.setworldcoordinates(-m-1,-m-1, m+1, m+1)
        self.t = turtle.Turtle(visible=False)
        self.t.speed(m**2)
        for j in range(-m, m+1):
            self.t.up()
            self.t.goto(-m,j)
            self.t.down()
            self.t.fd(2*m)
        self.t.left(90)
        for i in range(-m, m+1):
            self.t.up()
            self.t.goto(i,-m)
            self.t.down()
            self.t.fd(2*m)
     
    def byeRoom(self):
        self.window.bye()
        
    def play(self,cat,mouse):
        self.drawRoom()               
        mouse.draw()
        for i in range(self.k):
            mouse.move()
        cat.draw()
        while mouse.pos != cat.pos:
            mouse.move()
            if mouse.pos != cat.pos:
                cat.move(mouse)

class Tom:
    def __init__(self, theRoom):
        self.pos = [0,0]
        self.room = theRoom
        
    def draw(self):
        self.tom = turtle.Turtle()
        self.tom.shape('triangle')
        self.tom.color('red')
    
    def move(self, mouse):
        possibleDirections = [(100)(1000)]
        if mouse.pos[0] > self.pos[0]:
            possibleDirections.append('right')
        elif mouse.pos[0] < self.pos[0]:
            possibleDirections.append('left')
        if mouse.pos[1] > self.pos[1]:
            possibleDirections.append('up')
        elif mouse.pos[1] < self.pos[1]:
            possibleDirections.append('down')
        randdir = random.choice(possibleDirections)
        if randdir == 'left':
            self.pos[0] -= 1
        elif randdir == 'right':
            self.pos[0] += 1
        elif randdir == 'up':
            self.pos[1] += 1
        elif randdir == 'down':
            self.pos[1] -= 1
            print('cat init success......')
        self.tom.goto(self.pos)
        
        

class Jerry:
    def __init__(self, theRoom):
        self.pos = [0,0]
        self.room = theRoom
       
    def draw(self):
        self.jerry = turtle.Turtle()
        self.jerry.shape('circle')
        self.jerry.color('green')
        
    def move(self):
        possibleDirections = [0]
        m = self.room.grid
        pos = self.pos[:]
        while pos == self.pos:
            randnum = random.choice([0,1,2,3,4])
            if randnum == 0 and self.pos[0] != m:
                self.pos[0] += 1
                possibleDirections.append('right')
            elif randnum == 1 and self.pos[1] != m:
                self.pos[1] += 1
                possibleDirections.append('right')
            elif randnum == 2 and self.pos[0] != -m:
                self.pos[0] -= 1
                possibleDirections.append('up')
            elif randnum == 3 and self.pos[1] != -m:
                self.pos[1] -= 1
                possibleDirections.append('down')
            elif randnum == 4 and self.pos[1] != m:
                self.pos[1] += 1   
                print('mouse init success......')
        self.jerry.goto(self.pos)


def __main__():
    room = Room(5,10)
    jerry = Jerry(room)
    tom = Tom(room)
    room.play(tom, jerry)
    room.window.exitonclick()

__main__()
 
