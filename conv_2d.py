import numpy as np

def index(arr, value):
    if len(arr.shape)>1:
        return("The input array must be 1-d.")
    result = np.where(arr == value)[0]
    if result.size == 1:
        return(result[0])
    return(result)

arr=np.array([1,0,2,0,3,0,4,0,5,0,6,7,8,0])
#print(index(arr, 3))
#print(index(arr, 0))

def convolution2d(arr, filt,p):
    arrShape = arr.shape
    filShape = filt.shape
    pad = p
    if len(arrShape)!=2 or len(filShape)!=2 or filShape[0]!=filShape[1]\
    or arrShape[0]<filShape[0] or arrShape[1]<filShape[1]:
        return("Incorrect array shape.")
    resShape = (arrShape[0]-filShape[0]+2*pad, arrShape[1]-filShape[1]+2*pad)
    result = np.full(resShape,0)
    for i in range(resShape[0]):
        for j in range(resShape[1]):
            arrSub = arr[i:(i+filShape[0]-2),j:(j+filShape[1]-2)]
            x = np.zeros(filt.shape)
            #x = np.zeros_like(filt.shape)
            x[:arrSub.shape[0], :arrSub.shape[1]] = arrSub
            result[i,j] = np.sum(x * filt)
           
    return result
    
   


np.random.seed(1)
a = np.random.randint(0,3,(15,11))
print(a)
f = np.random.randint(0,2,(5,5))
print(f) 
r = convolution2d(a,f,1)
print(r)
