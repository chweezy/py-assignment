#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 18:52:57 2020

@author: mima
"""
""" uncomment the code below to get the csv file """
#import requests

#download_url = "https://ed-public-download.app.cloud.gov/downloads/Most-Recent-Cohorts-Scorecard-Elements.csv"
#target_csv_path = "cohorts.csv"

#response = requests.get(download_url)
#response.raise_for_status()    # Check that the request was successful
#with open(target_csv_path, "wb") as f:
 #   f.write(response.content)
#print("Download ready.")

import pandas as pd
import numpy 
import seaborn as sns
univ=pd.read_csv("cohorts.csv")

#random seed index of NUCLEOTODE ID 100654
"""this will give a random seed of 1000"""
df= pd.DataFrame(univ)
print(df.sample(n=1000, random_state =110565))

#resetting our index
df = df.drop([0, 3])
df = df.reset_index(drop=True)

#now lets arrange our data neatly
df.sort_index(inplace=True)
print(df)

#cleanly sorted data in form of univ1000
univ1000 = df.sort_index(inplace=True)

#calculating the confidence limit
from scipy.stats import sem, t
from scipy import mean
confidence = 0.95

df = pd.DataFrame(univ1000)
n = len(df)
m = mean(n)
h = 0.7503 * t.ppf((1 + confidence) / 2, n - 1)

start = m - h
print(start)